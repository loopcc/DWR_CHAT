package com.zhenzhigu.chat;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.directwebremoting.Container;
import org.directwebremoting.ScriptBuffer;
import org.directwebremoting.ScriptSession;
import org.directwebremoting.ServerContextFactory;
import org.directwebremoting.WebContextFactory;
import org.directwebremoting.event.ScriptSessionEvent;
import org.directwebremoting.event.ScriptSessionListener;
import org.directwebremoting.extend.ScriptSessionManager;
import org.directwebremoting.json.JsonUtil;
import org.directwebremoting.json.parse.JsonParseException;
import org.directwebremoting.json.types.JsonObject;

public class ChatServer {
	//记录所有在线的ScriptSession
	private final static List<ScriptSession> SESSIONS = new ArrayList<ScriptSession>();
	private final static SimpleDateFormat DF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	static{
		//得到DWR容器
		Container container = ServerContextFactory.get().getContainer();  
		//从DWR中得到会话管理器
        ScriptSessionManager manager = container.getBean(ScriptSessionManager.class);
        //创建一个会话监听器
        ScriptSessionListener ssl = new ScriptSessionListener() {
        	@Override
    		public void sessionCreated(ScriptSessionEvent ev) {
    			SESSIONS.add(ev.getSession());
    			System.out.println("用户接入:"+SESSIONS.size());
    		}
    		@Override
    		public void sessionDestroyed(ScriptSessionEvent ev) {
    			SESSIONS.remove(ev.getSession());
    			System.out.println("用户断开:"+SESSIONS.size());
    		}
    	};
    	//为管理器添加监听
    	manager.addScriptSessionListener(ssl);
	}
	
	/**
	 * 消息推送
	 * @param content
	 * @throws JsonParseException 
	 * @throws IOException 
	 */
	public void push(String content) throws JsonParseException, IOException{
		
		//把接收到的JSON字符串转为map形式
		Map<String, Object> map = JsonUtil.toSimpleObject(content);
		//设置消息的时间
		map.put("date", DF.format(new Date()) );
		
		//得到当前用户的ScriptSession
		ScriptSession seft = WebContextFactory.get().getScriptSession();
		
		for(ScriptSession session : SESSIONS){
			//设置消息是否当前用户
			map.put("isSelf", session.equals(seft));
			//把消息转为JSON格式的字符串
			String str = JsonUtil.toJson(map);
			
			//创建缓存脚本，执行指定function并传递参数
			ScriptBuffer script = new ScriptBuffer();
			script.appendCall("dwr.onmessage", str);
			//把脚本添加到会话中使其生效
			session.addScript(script);
		}
	}
}
